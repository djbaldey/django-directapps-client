# -*- coding: utf-8 -*-
#
#   Copyright 2016 Grigoriy Kramarenko <root@rosix.ru>
#
#   This file is part of DjangoDirectAppsClient.
#
#   DjangoDirectAppsClient is free software: you can redistribute it and/or
#   modify it under the terms of the GNU Affero General Public License
#   as published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   DjangoDirectAppsClient is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public
#   License along with DjangoDirectAppsClient. If not, see
#   <http://www.gnu.org/licenses/>.
#

from django.urls import path, re_path
from django.contrib.auth.views import logout
from directapps_client.views import index, ajaxtemplate

app_name = 'directapps_client'

urlpatterns = [
    path('', index, name='index'),
    path('logout', logout, name='logout'),
    re_path(r'^(?P<name>\w+\.html)$', ajaxtemplate, name='ajaxtemplate'),
]
