# -*- coding: utf-8 -*-
#
#   Copyright 2016 Grigoriy Kramarenko <root@rosix.ru>
#
#   This file is part of DjangoDirectAppsClient.
#
#   DjangoDirectAppsClient is free software: you can redistribute it and/or
#   modify it under the terms of the GNU Affero General Public License
#   as published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   DjangoDirectAppsClient is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public
#   License along with DjangoDirectAppsClient. If not, see
#   <http://www.gnu.org/licenses/>.
#
import json
from django.conf import settings

conf = getattr(settings, 'DIRECTAPPS_CLIENT', {})
# Словарь пользовательских AJAX шаблонов. Примеры:
# 'auth': 'auth' - шаблон приложения
# 'auth.user': 'auth-user' - шаблон коллекции объектов модели
# 'auth.user.object': 'auth-user-object' - шаблон работы с объектом
# 'auth.user.object.terminal': 'user_terminal' - шаблон реляции "Terminal"
CUSTOM_TEMPLATES = json.dumps(conf.get('CUSTOM_TEMPLATES', {}))
# Название проекта, которое префиксом отображается в заголовке страниц.
PROJECT_NAME = conf.get('PROJECT_NAME', '')
