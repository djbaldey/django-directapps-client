/*
 * links.js
 * 
 * Обработка ссылок на клиенте.
 * 
 * Зависит от:
 *  js/directapps.js,
 *  js/tabs.js
 *  js/tables.js
 *  js/utils.js
 * 
 */

/* Функция возвращает тип принадлежности хеша к DirectApps, ссылке на
 * внутренний идентификатор или любой другой внешний ресурс. */
function getHashType(hash) {
    if (hash.length > 1 && hash.indexOf('#!') != 0) return 'internal'
    else if (isDirectAppsLink(hash)) return 'directapps'
    else if (hash.indexOf('#!/') == 0) return 'external'
}

/* Функция запускает нужный обработчик при изменении хеша адреса. */
window.onhashchange = function() {
    var type = getHashType(location.hash);
    if (type) {
        if (type == 'directapps') openDirectApps()
        else if (type == 'internal') openInternal()
        else if (type == 'external') openExternal()
    }
}

/* Функция подгружает необходимые данные из DirectApps и после этого открывает
 * вкладку.
 */
function openDirectApps() {
    var links = directAppsLinks(location.hash),
        url = links.join('/')+'/',
        hash = '#!'+url;

    // Автоматическое исправление ссылки без перезагрузки страницы.
    if (location.hash != hash) {
        location.hash = hash;
    }
    var id = urlToId(hash.slice(2));
    var l = links.length;
    var htmlPane = function(html) {$('#pane-'+id).append(html)};

    if (l == 5) {
        loadScheme(links[1], links[2], links[4], function(app, model, relation) {
            if (existTab(id) && $('#table-'+id).length) {
                openTab(id)
            } else {
                $.getJSON(links.slice(0,4).join('/')+'/', function(object) {
                    // Отрисовка объекта и реляции
                    var text = object.display_name+': '+relation.display_name,
                        ctrl = getController(app, model);
                    createTab(id, text, hash);
                    ajaxTemplate('model', {
                        id: id,
                        ctrl: ctrl,
                        object: object,
                        relation: relation
                    }, function(html) {
                        htmlPane(html);
                        // из tables.js TODO: заменить событием
                        renderRelation(id, ctrl, links[3], links[4], 1, true);
                        // Извещаем другие части приложения триггер-сигналом.
                        $(document).trigger(
                            'relation_open', id, ctrl, object, relation);
                    });
                    openTab(id)
                })
            }
        })
    } else if (l == 4) {
        loadScheme(links[1], links[2], null, function(app, model) {
            var ctrl = getController(app, model);
            var create = Boolean(url == ctrl.newobject_url);
            if (existTab(id) && $('#pane-'+id).html()) {
                openTab(id);
            } else if (create) {
                var object = ctrl.newObject();
                // Отрисовка нового объекта
                createTab(id, object.display_name, hash);
                ajaxTemplate('object', {
                    id: id,
                    ctrl: ctrl,
                    object: object
                }, function(html) {
                    htmlPane(html);
                    // Извещаем другие части приложения триггер-сигналом.
                    $(document).trigger('object_open', id, ctrl, object);
                });
                openTab(id)
            } else {
                $.getJSON(url, function(object) {
                    // Отрисовка объекта
                    createTab(id, object.display_name, hash);
                    ajaxTemplate('object', {
                        id: id,
                        ctrl: ctrl,
                        object: object
                    }, function(html) {
                        htmlPane(html);
                        // Извещаем другие части приложения триггер-сигналом.
                        $(document).trigger('object_open', id, ctrl, object);
                    });
                    openTab(id)
                })
            }
        })
    } else if (l == 3) {
        loadScheme(links[1], links[2], null, function(app, model) {
            if (existTab(id) && $('#table-'+id).length) {
                openTab(id)
            } else {
                // Отрисовка модели объектов
                createTab(id, model.display_name, hash);
                var ctrl = getController(app, model);
                ajaxTemplate('model', {
                    id: id,
                    ctrl: ctrl,
                    object: null,
                    relation: null
                }, function(html) {
                    htmlPane(html);
                    // из tables.js TODO: заменить событием
                    renderModel(id, ctrl, 1, true);
                    // Извещаем другие части приложения триггер-сигналом.
                    $(document).trigger('model_open', id, ctrl);
                })
                openTab(id)
            }
        })
    } else if (l == 2) {
        loadScheme(links[1], null, null, function(app) {
            if (existTab(id) && $('#pane-'+id).html()) {
                openTab(id)
            } else {
                // Отрисовка приложения
                createTab(id, app.display_name, hash);
                ajaxTemplate('app', {id: id, app: app}, function(html) {
                    htmlPane(html);
                    // Извещаем другие части приложения триггер-сигналом.
                    $(document).trigger('app_open', id, app);
                });
                openTab(id)
            }
        })
    } else {
        console.debug('apps url')
    }
}

function iframeReady(id, iframe) {
    var name = $('#iframe-' + id).contents().find("title").text() || id;
    renameTab(id, name);
}

/* Функция подгружает необходимую внешнюю страницу и после этого открывает
 * вкладку.
 */
function openExternal() {
    var hash = location.hash,
        url = hash.slice(2),
        id = urlToId(url);
    var created = createTab(id, id, hash);
    ajaxTemplate('iframe', {id: id, url: url}, function(html) {
        $('#pane-'+id).html(html);
        // Извещаем другие части приложения триггер-сигналом.
        $(document).trigger('open_external', id, created, $('#pane-'+id));
    })
    openTab(id)
}

/* Функция открывает встроенную страницу и вкладку для неё. */
function openInternal() {
    var hash = location.hash,
        id = urlToId(hash.slice(1)),
        created = createTab(id, (window.TRANSLATIONS || {})[id] || id, hash),
        $pane = $('#pane-'+id);
    if (created || (id == 'dashboard' && !$pane.html())) {
        $pane.html(window.TEMPLATES[id]());
    }
    openTab(id)
    // Извещаем другие части приложения триггер-сигналом.
    $(document).trigger('open_internal', id, created, $pane);
}


