/*
 * меню.js
 * 
 * Функции для загрузки и отображения меню приложения.
 * 
 * Зависит от:
 *  js/localstorage.js,
 *  js/templates.js
 *  js/utils.js
 * 
 */

/* Функция отфильтровывает пункты меню, скрывая неподходящие
 * и отображая необходимые.
 */
function filterMenu(e) {
    var v = this.value.toUpperCase();
    //~ window.CONFIG['menu-filter-value'] = this.value || '';
    //~ toLocalStorage('config', window.CONFIG);
    if (v) {
        $.each(window.CACHE.scheme.apps, function(index, app) {
            // Если найдено приложение, то отображаем все её модели
            if (app.name.toUpperCase().indexOf(v) >-1
            || app.display_name.toUpperCase().indexOf(v) >-1) {
                $('#sidebar-menu>ul>li[data-name='+app.name+']:not(:visible)').show();
                $('#sidebar-menu>ul>li[data-name='+app.name+'] li:not(:visible)').show();
                return
            }
            // Отображаем только модели, которые соответствуют запросу
            var show = false;
            $.each(app.models, function(i, model) {
                if (model.name.toUpperCase().indexOf(v) >-1
                || model.display_name.toUpperCase().indexOf(v) >-1) {
                    $('#sidebar-menu>ul>li[data-name='+app.name
                      +'] li[data-name='+model.name+']:not(:visible)').show();
                    show = true
                } else {
                    $('#sidebar-menu>ul>li[data-name='+app.name
                      +'] li[data-name='+model.name+']:visible').hide();
                }
            })
            if (show) {
                $('#sidebar-menu>ul>li[data-name='+app.name+']:not(:visible)').show()
            } else {
                $('#sidebar-menu>ul>li[data-name='+app.name+']:visible').hide()
            }
        });
    } else {
        $('#sidebar-menu li:not(:visible)').show()
    }
}

/* Функция загружает и отрисовывает меню. */
function loadMenu(callback) {
    var cache = window.CACHE;
    $.getJSON(APPS_URL, function(scheme) {
        var apps;
        if (!cache.scheme || cache.scheme.checksum != scheme.checksum) {
            cache.scheme = scheme;
            apps = scheme.apps;
            apps.sort(sortDisplayName);
            $.each(apps, function(i,val) {val.models.sort(sortDisplayName)});
            toLocalStorage('cache', cache);
        } else {
            apps = cache.scheme.apps;
        }
        $('#sidebar-menu').html(window.TEMPLATES.menu({apps:apps}));
        callback();
    });
}

/* Инициализация */
function menuInit() {
    // Запускаем фильтр по главному меню.
    $('#sidebar-menu-filter input')
        //~ .val(window.CONFIG['menu-filter-value'] || '')
        .on('keyup', filterMenu).keyup();
}
