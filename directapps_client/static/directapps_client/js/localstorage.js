/*
 * localstorage.js
 * 
 * Обработка localStorage.
 * 
 */

/* Проверка возможности исполнения localStorage и конвертора JSON. */
function checkLocalStorage() {
    if (!window.localStorage || !window.JSON || !window.JSON.stringify) {
        console.error('localStorage or JSON.stringify is undefined');
        return false
    }
    return true
}

/* Функция добавления настроек в localStorage.
 * `fkey` - полный ключ к значению, когда задан, то `key` уже не
 * используется.
 */
function toLocalStorage(key, val, fkey) {
    if (!checkLocalStorage()) return;
    var k = fkey || window.SETTINGS_KEY+key,
        v = JSON.stringify(val);
    localStorage.setItem(k, v);
    return [k, v]
}

/* Функция получения настроек из localStorage.
 * `fkey` - полный ключ к значению, когда задан, то `key` уже не
 * используется.
 */
function fromLocalStorage(key, fkey) {
    if (!checkLocalStorage()) return;
    var val = localStorage.getItem(fkey || window.SETTINGS_KEY+key);
    try {
        return JSON.parse(val)
    } catch (e) {
        return val
    }
}
