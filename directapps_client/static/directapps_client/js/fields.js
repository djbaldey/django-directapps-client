/*
 * fields.js
 * 
 * Объекты полей и правила работы с ними.
 * 
 * LOOKUPS = ["exact", "iexact", "gt", "gte", "lt", "lte", "isnull",
 *   "regex", "iregex", "in", "range", "search", "contains", "icontains",
 *   "startswith", "istartswith", "endswith", "iendswith", "year", "month",
 *   "day", "week_day", "hour", "minute", "second"]
 */

function Field(lookups) {
    var self = this,
        TS = TRANSLATIONS.lookups || {},
        defaults = ['exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull'],
        times = ['year', 'month', 'day', 'week_day', 'hour', 'minute', 'second'],
        subtimes = ['exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range'];

    this.lookups = lookups || defaults;
    this.variants = [];
    this.time_lookups = [];
    this.labels = {'isnull': [TRANSLATIONS['no'], TRANSLATIONS['yes']]};

    var add = function(l, dn) { self.variants.push([l, dn]) };
    $.each(self.lookups, function(i, l) {
        if (times.indexOf(l) > -1 || ['date', 'time'].indexOf(l) > -1) {
            $.each(subtimes, function(n, sub) {
                var s = l + '__' + sub;
                self.time_lookups.push(s);
                add(s, TS[l] + ' ' + TS[sub])
            })
        } else {
            add(l, TS[l]);
        }
    })

    this.getDefault = function(value) {return value}
}

var FIELDS = {Field: new Field()};
FIELDS.AutoField = new Field([
    'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range']);
FIELDS.AutoField.is_number = true;
FIELDS.BigAutoField = FIELDS.AutoField;

FIELDS.BooleanField = new Field(['exact', 'isnull']);
FIELDS.BooleanField.is_bool = true;
FIELDS.BooleanField.labels['exact'] = [TRANSLATIONS['false'], TRANSLATIONS['true']];
FIELDS.NullBooleanField = FIELDS.BooleanField;

FIELDS.IntegerField = new Field([
    'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull']);
FIELDS.IntegerField.is_number = true;
FIELDS.IntegerField.getDefault = function(value) {return Number(value)}
FIELDS.BigIntegerField = FIELDS.IntegerField;
FIELDS.DecimalField = FIELDS.IntegerField;
FIELDS.DurationField = FIELDS.IntegerField;
FIELDS.FloatField = FIELDS.IntegerField;
FIELDS.PositiveIntegerField = FIELDS.IntegerField;
FIELDS.PositiveSmallIntegerField = FIELDS.IntegerField;
FIELDS.SmallIntegerField = FIELDS.IntegerField;

FIELDS.ForeignKey = new Field([
    'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull']);
FIELDS.ForeignKey.is_relation = true;
FIELDS.ManyToManyField = FIELDS.ForeignKey;
FIELDS.OneToOneField = FIELDS.ForeignKey;

FIELDS.CharField = new Field([
    'contains', 'icontains', 'search', 'startswith', 'istartswith', 'endswith',
    'iendswith', 'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull']);
FIELDS.CharField.is_text = true;
FIELDS.EmailField = FIELDS.CharField;
FIELDS.FileField = FIELDS.CharField;
FIELDS.FieldFile = FIELDS.CharField;
FIELDS.FilePathField = FIELDS.CharField;
FIELDS.ImageField = FIELDS.CharField;
FIELDS.SlugField = FIELDS.CharField;
FIELDS.TextField = FIELDS.CharField;
FIELDS.URLField = FIELDS.CharField;
FIELDS.UUIDField = FIELDS.CharField;

FIELDS.JSONField = new Field([
    'exact', 'isnull', 'contains', 'icontains', 'search']);
FIELDS.JSONField.is_json = true;
FIELDS.StrictJSONField = FIELDS.JSONField;

FIELDS.BinaryField = new Field(['exact', 'isnull']);

FIELDS.DateField = new Field([
    'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull',
    'year', 'month', 'day', 'week_day']);
FIELDS.DateField.is_date = true;
FIELDS.DateField.format = function(cond) {return 'YYYY-MM-DD'};
FIELDS.DateField.getDefault = function(value) {
    if (value == 'auto') {
        return moment().format('YYYY-MM-DD')
    }
    return moment(value).format('YYYY-MM-DD')
}

FIELDS.DateTimeField = new Field([
    'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull', 'date', 'time',
    'year', 'month', 'day', 'week_day', 'hour', 'minute', 'second']);
FIELDS.DateTimeField.is_date = true;
FIELDS.DateTimeField.is_time = true;
FIELDS.DateTimeField.format = function(cond) {
    if (cond.startsWith('date')) return 'YYYY-MM-DD';
    if (cond.startsWith('time')) return 'HH:mm:ss';
    return 'YYYY-MM-DD HH:mm:ss'
};
FIELDS.DateTimeField.getDefault = function(value) {
    if (value == 'auto') {
        return moment().format()
    }
    return moment(value).format()
}

FIELDS.TimeField = new Field([
    'exact', 'gt', 'gte', 'lt', 'lte', 'in', 'range', 'isnull',
    'hour', 'minute', 'second']);
FIELDS.TimeField.is_time = true;
FIELDS.TimeField.format = function(cond) {return 'HH:mm:ss'};
FIELDS.TimeField.getDefault = function(value) {
    if (value == 'auto') {
        return moment().format('HH:mm:ss')
    }
    return moment(value).format('HH:mm:ss')
}


var FIELDS_ORDERING_INDEX = [
    'AutoField',
    'BigAutoField',
    'UUIDField',

    'DateTimeField',
    'DateField',
    'TimeField',

    'EmailField',
    'SlugField',
    'URLField',
    'CharField',

    'IntegerField',
    'BigIntegerField',
    'PositiveIntegerField',
    'PositiveSmallIntegerField',
    'SmallIntegerField',
    'FloatField',
    'DecimalField',
    'DurationField',

    'BooleanField',
    'NullBooleanField',

    'ForeignKey',
    'OneToOneField',
    'ManyToManyField',

    'ImageField',
    'FileField',
    'FilePathField',

    'TextField',
    'JSONField',
    'StrictJSONField',

    'BinaryField',
]


function testImportantField(f) {
    return !!(
        f.primary_key ||
        f.unique ||
        !(f.has_default || f.blank || f['null'])
    );
}


/* Сортировка полей для редакторов объектов. */
function sortFields(a, b) {
    // сперва нередактируемые поля
    if (a.readonly != b.readonly) {
        return (!!a.readonly > !!b.readonly) ? -1 : 1;
    }
    // затем обязательные поля
    var ai = testImportantField(a),
        bi = testImportantField(b);
    if (ai != bi) {
        return (!!ai > !!bi) ? -1 : 1;
    }
    // сортировка по типам полей и названиям
    ai = FIELDS_ORDERING_INDEX.indexOf(a.type);
    bi = FIELDS_ORDERING_INDEX.indexOf(b.type);
    if (ai == bi) {
        return (a.display_name > b.display_name) ? 1 : -1;
    } else {
        return (ai > bi) ? 1 : -1;
    }
}


/* Разделяет список полей по группам. */
function formFieldSets(fields) {
    var fs = {};
    fields.sort(sortFields);
    for (var i = 0; i < fields.length; i++) {
        var name, f = fields[i], type = FIELDS[f.type] || Field;
        f._type = type;
        if (f.readonly) {
            name = 'readonly'
        } else {
            name = 'editable'
        }
        var fset = fs[name] || [];
        fset.push(f);
        fs[name] = fset;
    }
    return fs
}


/* Устанавливает поиск для поля внешнего ключа */
function setSelectizeField(e) {
    var $select = $(this),
        data = $select.data(),
        ctrl = getController(data.ctrl),
        fkey = $select.attr('name');

    $select.removeClass('selectize-is-not-set').selectize({
        load: function(query, callback) {
            // if (!query.length) return callback();
            var url = ctrl.model.url + '_fkey/',
                params = {q: query, f: fkey};
            $.getJSON(url, params, callback);
        },
        valueField: 'pk',
        labelField: 'display_name',
        searchField: 'display_name',
        create: false,
        render: {
            option: function(item, escape) {
                return '<div>' +
                    '<span class="pull-right"> #' + item.pk + ' </span>' +
                    '<span>' + item.display_name + '</span>' +
                    '</div>'
                ;
            }
        },
    })
}


/* Устанавливает пикер для полей дат и времени */
function setDateTimePickerField(e) {
    var $input = $(this),
        data = $input.data();
    $input.removeClass('datetimepicker-is-not-set')
          .datetimepicker({format: data.format})
          .data("DateTimePicker")
          .show();

    $input.on("dp.change", function(e) {
        var sv = moment(this.value, data.format).toISOString();
        $(this).data('server_value', sv)
    })
}


/*
function changeFormControlFile(e) {
    var $btn = $(this),
        text = (this.files[0] || {}).name,
        default_text = $btn.data('default_text') || 'Browse';
    $btn.prev('span').text(text || default_text)
}
*/


/* Обработка подтверждения формы объекта. */
function submitFormObject(e) {
    var $form = $(this),
        id = $form.data('id'),
        url = $form.attr('action'),
        method = ($form.attr('method') || 'post').toLowerCase(),
        formData = new FormData(this);
    // Обычная форма не передаёт M2M поле, если оно не заполнено,
    // к тому же нам тут необходимо обрабатывать серверные значения дат,
    // времени, чисел и прочего, что может быть локализовано для отображения
    // пользователю.
    $form.find('[name]').each(function(i, item) {
        var $el = $(item),
            server_value = $el.data('server_value');
        // Если есть поля со специальным серверным значением, то берём его
        if (typeof server_value != 'undefined') {
            formData.set(item.name, server_value)
        }
        // Если в форме есть M2M поля, то нужно для них заменить null
        else if ($el.attr('multiple') && !$el.val()) {
            formData.set(item.name, [])
        }
        // Для правильной обработки чекбоксов почему-то нужно
        // переустановить их значения. 
        else if ($el.attr('type') == 'checkbox') {
            formData.set(item.name, $el.prop('checked'))
        }
    })
    $.ajax({
        url: url,
        type: method,
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'JSON',
        success: function(data) {
            location.hash = '#!' + data.url;
            closeTab(id)
        },
    })
    e.preventDefault();
}


/* Обработка удаления объекта. */
function clickDeleteObject(e) {
    var $el = $(this);
    result = confirm($el.data('question') || 'Are you shure?');
    if (result) {
        $.ajax({
            url: $el.attr('href') || $el.attr('url'),
            type: 'DELETE',
            dataType: 'JSON',
            success: function(data) {closeTab($el.data('id'))},
        })
    }
    e.preventDefault();
}


/* Инициализация */
function fieldsInit() {
    $('body').on('focus', 'form.form-object .datetimepicker-is-not-set', setDateTimePickerField)
    $('body').on('focus', 'form.form-object .selectize-is-not-set', setSelectizeField)
    $('body').on('change', 'form.form-object .selectize-is-not-set', setSelectizeField)
    // $('body').on('change', 'form.form-object .form-control-file', changeFormControlFile)
    $('body').on('submit', 'form.form-object', submitFormObject)
    $('body').on('click', 'a.delete-object', clickDeleteObject)
    $(document).on('object_open', function(e, id, ctrl, object) {
        $('form.form-object .selectize-is-not-set').change()
    });
}
