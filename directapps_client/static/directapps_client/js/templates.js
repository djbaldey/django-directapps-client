/*
 * templates.js
 *
 * Обработка шаблонов Underscore.
 *
 */


/* Функция компилирует все встроенные шаблоны и возвращает объект TEMPLATES. */
function internalTemplates() {
    var t = {};
    t.menu = _.template($('#template-menu').html());
    t.tab = _.template($('#template-tab').html());
    t.pane = _.template($('#template-pane').html());
    t.row = _.template($('#template-row').html());
    t.filter_values = _.template($('#template-filter-values').html());
    t.filter = _.template($('#template-filter').html());
    t.dashboard = _.template($('#template-dashboard').html());
    t.history = _.template($('#template-history').html());
    t.favorites = _.template($('#template-favorites').html());
    t.settings = _.template($('#template-settings').html());
    t.help = _.template($('#template-help').html());
    return t
}


/* Функция подгружает необходимый шаблон, рендерит его на основании данных
 * и полученый HTML передаёт callback-функции.
 */
function ajaxTemplate(name, data, cb) {
    var TS = window.TEMPLATES,
        f = function() { cb(TS[name](data)) };
    if (TS[name]) {
        f()
    } else {
        $.get(name+'.html', function(page) {
            TS[name] = _.template(page);
            f()
        })
    }
}
