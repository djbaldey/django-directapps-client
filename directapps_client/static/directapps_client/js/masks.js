/*
 * masks.js
 * 
 * Обработка масок полей ввода.
 * 
 */

function masksKeyUp(e) {
    var $target = $(e.target),
        data = $target.data(),
        keycode = e.which,
        alt = e.altKey,
        ctrl = e.ctrlKey,
        shift = e.shiftKey,
        mask = getMask(data.mask);
    if (mask) {
        var val = $target.val();
        for (var i=0;i<mask.length;i++) {
            var m = mask[i],
                v = val[i];
            //~ if (v && !Number(v)) {
                //~ val[i] = m
            //~ }
            val[i] = m
        }
        $target.val(val)
    }
}

function getMask(type) {
    if (type == 'DateTimeField') {
        return '____-__-__ __:__:__'
    } else if (type == 'DateField') {
        return '____-__-__'
    } else if (type == 'TimeField') {
        return '__:__:__'
    }
    return ''
}

/* Инициализация */
function masksInit() {
    $('body').on('keyup', '.maskinput', masksKeyUp)
}

