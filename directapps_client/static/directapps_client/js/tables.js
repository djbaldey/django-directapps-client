/*
 * tables.js
 * 
 * Прорисовка различных таблиц.
 * 
 * Зависит от:
 *  js/directapps.js,
 *  js/pagination.js,
 * 
 */

/* Прорисовывает таблицу данных. */
function renderTable(id, ctrl, config, rel, data, replace, start_time) {
    var $tbody = $('#table-'+id+' tbody'),
        limit = config.limit,
        length = data.objects.length,
        row = window.TEMPLATES.row,
        objects = data.objects;

    if (replace) $tbody.html('');
    // Паждинатор может возвращать больше в конце таблицы.
    if (length > limit || !replace) limit = length;

    var has_open_object = testPermission(rel? rel.relation : ctrl.name),
        columns = ctrl.visibleColumns(rel);;

    for (var i=0; i<limit; i++) {
        var object = objects[i],
            d = {
                index: i,
                ctrl: ctrl,
                config: config,
                rel: rel,
                object: object,
                columns: columns,
                has_open_object: has_open_object
            };
        $tbody.append(row(d));
    }
    renderPage(id, config.table_type, data.page, data.num_pages);
    var info = data.info || {};
    info[TRANSLATIONS['request_time']] = (new Date() - start_time) + 'ms';
    info[TRANSLATIONS['request_weigth']] = data.num_pages*config.limit;
    renderInfo(id, info);
}

/* Прорисовывает информацию к таблице. */
function renderInfo(id, info) {
    var html = '';
    $.each(info, function(k,v) {
        html += '<dt>' + k + '</dt><dd>' + v + '</dd>'
    })
    $('#info-'+id).html(html);
}

/* Получает и прорисовывает данные модели. */
function renderModel(id, ctrl, page, replace) {
    var config = ctrl.config,
        limit = config.limit;
    if (page > 0) {
        ctrl.pages['_'] = page
    }
    var start_time = new Date();
    ctrl.get().done(function(data, status, xhr) {
        renderTable(id, ctrl, config, null, data, replace, start_time)
    });
}

/* Получает и прорисовывает данные реляции. */
function renderRelation(id, ctrl, pk, name, page, replace) {
    var rel = ctrl.getRel(name),
        config = ctrl.getRelConfig(rel);
    if (page > 0) {
        ctrl.pages[name] = page
    }
    var start_time = new Date();
    ctrl.getRelation(pk, name).done(function(data) {
        renderTable(id, ctrl, config, rel, data, replace, start_time)
    });
}

/* Прорисовывает один фильтр. */
function renderFilter(id, ctrl, config, object, relation, filter) {
    return window.TEMPLATES.filter({
        id: id,
        ctrl: ctrl,
        config: config,
        object: object,
        relation: relation,
        filter: filter
    })
}

/* Прорисовывает значения фильтра. */
function renderFilterValues(id, ctrl, config, object, relation, filter) {
    return window.TEMPLATES.filter_values({
        id: id,
        ctrl: ctrl,
        config: config,
        object: object,
        relation: relation,
        filter: filter
    })
}


