/*
 * handlers.js
 * 
 * Обработчики событий.
 * 
 * Зависит от:
 *  js/directapps.js,
 *  js/tabs.js
 *  js/tables.js
 *  js/utils.js
 * 
 */


/* Функция обработчика фильтра текстового поиска.*/
function daFilterChar(e) {
    /* Общая задержка для стека поисковых запросов. */
    this.delay = this.delay || (function() {
        var tid = 0;
        return function(callback, ms) {
            clearTimeout(tid);
            tid = setTimeout(callback, ms);
        };
    })();

    var fname = this.name,
        data = $(this).data(),
        ctrl = getController(data.ctrl),
        relation = data.relation,
        pk = data.object,
        id = data.id,
        min = Number(data.minimum) || 0, // minimum chars for query
        len = this.value.length,
        config = relation ? ctrl.getRelConfig(ctrl.getRel(relation)) : ctrl.config,
        old = getFilterByName(config, fname) || {},
        page = 1,
        replace = true;

    if (fname && this.value != old.value && (!len || len >= min)) {
        if (!this.value) {
            deleteFilterByName(config, fname)
        } else {
            old = getFilterByName(config, fname, true);
            old.value = this.value
        }
        saveConfig();
        if (relation) {
            this.delay(function() {
                renderRelation(id, ctrl, pk, relation, page, replace)
            }, 300)
        } else {
            this.delay(function() {
                renderModel(id, ctrl, page, replace)
            }, 300)
        }
    }
}

/* Обработчик булевого фильтра. */
function daFilterBool(e) {
    //~ e.preventDefault();
    var data = $(this).data();
    console.log(data)
}

/* Обработчик добавления фильтра. */
function daFilterAdd(e) {
    e.preventDefault();
    var data = $(this).data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config;

    $(this).blur();

    var html = window.TEMPLATES.filter({
        id: data.id,
        ctrl: ctrl,
        object: pk,
        relation: rel,
        filter: {id: generatorID('filter')}
    })
    colorFiltersBtn(id, ctrl, rel);
    $('#pane-filters-'+data.id+' .panel-body table tbody').append(html)
}

/* Обработчик изменения поля фильтра. */
function daFilterField(e) {
    e.preventDefault();
    var data = $(this).data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter, true),
        $condition = $('.da-filter-condition[data-filter="'+filter.id+'"]'),
        $values = $('.da-filter-values[data-filter="'+filter.id+'"]');

    $(this).blur();
    $condition.val('').find('option:not(:first)').remove();
    $values.html('');

    var vals, field, html = '';
    if (this.value) {
        vals = this.value.split('/');
        filter.field = vals[0];
        filter.type = vals[1] || 'Field';
        field = window.FIELDS[filter.type] || window.FIELDS.Field;
        $.each(field.variants, function(i, item) {
            html += '<option value="'+item[0]+'">'+item[1]+'</option>'
        })
    } else {
        delete filter.field;
        delete filter.type;
    }
    delete filter.active;
    delete filter.name;
    delete filter.value;
    saveConfig();
    colorFiltersBtn(id, ctrl, rel);
    $condition.append(html);
}

/* Обработчик изменения условия фильтра. */
function daFilterCondition(e) {
    e.preventDefault();
    var data = $(this).data(),
        value = $(this).val(),
        filter_id = data.filter,
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, filter_id, true);

    $(this).blur();
    var html = '';
    delete filter.active;
    delete filter.name;
    delete filter.value;
    if (this.value) {
        filter.condition = this.value;
        setFilterName(filter);
        html = renderFilterValues(id, ctrl, config, pk, rel, filter);
    } else {
        delete filter.condition;
    }
    saveConfig();
    colorFiltersBtn(id, ctrl, rel);
    $('.da-filter-values[data-filter="'+filter.id+'"]').html(html)
}

/* Обработчик инвертирования фильтра. */
function daFilterInverse(e) {
    e.preventDefault();
    var data = $(this).data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter, true),
        page = 1,
        replace = true;
    filter.inverse = !filter.inverse;
    setFilterName(filter);
    $(this).blur().toggleClass('active');
    saveConfig();
    if (relation) {
        renderRelation(id, ctrl, pk, relation, page, replace)
    } else {
        renderModel(id, ctrl, page, replace)
    }
}

/* Обработчик запоминания фильтра. */
function daFilterFix(e) {
    e.preventDefault();
    var data = $(this).data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter, true);
    filter.fix = !filter.fix;
    $(this).blur().toggleClass('active');
    saveConfig()
}

/* Обработчик удаления фильтра. */
function daFilterRemove(e) {
    e.preventDefault();
    var data = $(this).data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter),
        page = 1,
        replace = true;

    deleteFilterById(config, data.filter);
    $('#'+data.filter).remove();
    saveConfig();
    colorFiltersBtn(id, ctrl, rel);

    if (filter && filter.name && filter.active != false) {
        if (relation) {
            renderRelation(id, ctrl, pk, relation, page, replace)
        } else {
            renderModel(id, ctrl, page, replace)
        }
    }
}

/* Обработчик изменения количества на одной странице таблицы. */
function daLimit(e) {
    e.preventDefault();
    var data = $(this).data(),
        ctrl = getController(data.ctrl),
        relation = data.relation,
        pk = data.object,
        id = data.id,
        limit = data.limit,
        config = relation ? ctrl.getRelConfig(ctrl.getRel(relation)) : ctrl.config,
        page = 1,
        replace = true;
    if (config.limit != limit) {
        $('#limit-selector-'+id).text(limit);
        config.limit = limit;
        saveConfig();
        if (relation) {
            renderRelation(id, ctrl, pk, relation, page, replace)
        } else {
            renderModel(id, ctrl, page, replace)
        }
    }
}

/* Обработчик изменения сортировки таблицы. */
function daSort(e) {
    e.preventDefault();
    var rm, add, data = $(this).data(),
        column = data.column,
        sorting = data.sorting,
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        mc = ctrl.mc(relation),
        page = 1,
        replace = true;

    $(this).blur();

    var state0 = ctrl.changeOrdering(column, rel),
        state1 = getOrdering(mc[0], mc[1], column);

    if (!config.multiordering) {
        $('#table-'+id)
            .find('th.da-sort i.fa-sort-asc, th.da-sort i.fa-sort-desc')
            .removeClass('fa-sort-asc')
            .removeClass('fa-sort-desc')
            .addClass('fa-sort');
    }

    if (state0 != state1) {
        if (state1 == 'asc') {
            $(this).find('i.fa').removeClass('fa-sort').addClass('fa-sort-asc');
        } else if (state1 == 'desc') {
            $(this).find('i.fa').removeClass('fa-sort-asc').addClass('fa-sort-desc');
        } else {
            $(this).find('i.fa').removeClass('fa-sort-desc').addClass('fa-sort');
        }
        if (relation) {
            renderRelation(id, ctrl, pk, relation, page, replace)
        } else {
            renderModel(id, ctrl, page, replace)
        }
    }
}

/* Обработчик кликов на страницах. */
function daPagination(e) {
    e.preventDefault();
    var $this = $(this),
        page = $this.data('page'),
        data = $this.parents('[id^=pager-]').data(),
        ctrl = getController(data.ctrl),
        relation = data.relation,
        pk = data.object,
        id = data.id,
        config = relation ? ctrl.getRelConfig(ctrl.getRel(relation)) : ctrl.config,
        replace = true;
    if (config.table_type == 'stack') {
        replace = false
    }
    if (relation) {
        renderRelation(id, ctrl, pk, relation, page, replace)
    } else {
        renderModel(id, ctrl, page, replace)
    }
}

/* Обработчик кликов на рефреше. */
function daRefresh(e) {
    e.preventDefault();
    var data = $(this).data(),
        ctrl = getController(data.ctrl),
        relation = data.relation,
        pk = data.object,
        id = data.id,
        page,
        config = relation ? ctrl.getRelConfig(ctrl.getRel(relation)) : ctrl.config,
        replace = true;
    $(this).blur();
    if (config.table_type == 'stack') {
        page = 1
    }
    if (relation) {
        renderRelation(id, ctrl, pk, relation, page, replace)
    } else {
        renderModel(id, ctrl, page, replace)
    }
}

/* Обработчик кликов на переключателе типа таблицы. */
function daTableType(e) {
    e.preventDefault();
    var data = $(this).data(),
        ctrl = getController(data.ctrl),
        relation = data.relation,
        id = data.id,
        config = relation ? ctrl.getRelConfig(ctrl.getRel(relation)) : ctrl.config;

    $(this).blur();

    if (config.table_type == 'stack') {
        config.table_type = undefined
    } else if (config.table_type == 'pager') {
        config.table_type = 'stack'
    } else {
        config.table_type = 'pager'
    }
    saveConfig();
    reopenTab(id);
}

/* Обработчик события клика на кнопках фильтров и колонок. */
function simpleToggle(e) {
    e.preventDefault();
    var data = $(this).data(),
        id = data['toggle'];
    $(this).blur().toggleClass('active');
    //~ if (!window.SIMPLE_TOGGLES) window.SIMPLE_TOGGLES = {};
    var S = window.SIMPLE_TOGGLES,
        $id = $('#'+id);
    if ($id.hasClass('active')) {
        delete S[id]
    } else {
        S[id] = true
    }
    toLocalStorage('simple_toggles', S)
    $id.toggleClass('active')
        .siblings('.active[data-group="'+data.group+'"]')
        .each(function(i,item) {
            var _id = $(item).removeClass('active').attr('id');
            delete S[_id];
        })
}

/* Обработчик кликов на переключателе видимости колонки таблицы. */
function daColumnVisible(e) {
    e.preventDefault();
    var data = $(this).data(),
        index = Number(data.index),
        column = data.column,
        id = data.id,
        ctrl = getController(data.ctrl),
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config;

    $(this).blur();

    if (index > -1) {
        // выключаем колонку
        config.visible_columns.splice(index, 1)
    } else {
        // включаем колонку
        var m = rel || ctrl.model,
            l = m.columns.length;
        for (var i=0;i<l;i++) {
            var col = m.columns[i];
            if (col.name == column) {
                config.visible_columns.splice(0, 0, col);
                break
            }
        }
    }
    saveConfig();
    reopenTab(id);
}

/* Обработчик поля редактирования названия колонки таблицы. */
function daColumnName(e) {
    e.preventDefault();
    var data = $(this).data(),
        column = data.column,
        id = data.id,
        ctrl = getController(data.ctrl),
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        val = this.value,
        $col = $('#table-'+id+' th[data-column="'+column+'"] span.display-name');

    if (!val) {
        delete config.colnames[column];
        var m = rel || ctrl.model,
            l = m.columns.length,
            v;
        for (var i=0;i<l;i++) {
            var col = m.columns[i];
            if (col.name == column) {
                v = col.display_name;
                break
            }
        }
        $col.text(v);
        this.value = v;
    } else {
        config.colnames[column] = val;
        $col.text(val);
    }
    saveConfig();
}

/* Обработчик поля установки ширины колонки таблицы. */
function daColumnWidth(e) {
    e.preventDefault();
    var data = $(this).data(),
        column = data.column,
        id = data.id,
        ctrl = getController(data.ctrl),
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        val = Number(this.value) || null,
        $col = $('#table-'+id+' th[data-column="'+column+'"]');

    if (!val) {
        delete config.colwidth[column];
        this.value = '';
        $col.css('width', 'auto').css('min-width', '45px');
    } else {
        if (val < 45) {
            val = 45;
            this.value = val;
        }
        config.colwidth[column] = val;
        $col.css('width', val+'px').css('min-width', val+'px');
    }
    saveConfig();
}

/* Инициализация */
function handlersInit() {
    // Биндинги на тело
    $('body')
        /* Keyup on character filters */
        .on('keyup', '.da-filter-char,.da-filter-text', daFilterChar)
        /* change on boolean filters */
        .on('change', '.da-filter-bool', daFilterBool)
        /* add one filter to panel */
        .on('click', '.da-filter-add', daFilterAdd)
        /* Change field of filter */
        .on('change', '.da-filter-field', daFilterField)
        /* Change condition of filter */
        .on('change', '.da-filter-condition', daFilterCondition)
        /* Сlick on inverse filter */
        .on('click', '.da-filter-inverse', daFilterInverse)
        /* Сlick on fix filter */
        .on('click', '.da-filter-fix', daFilterFix)
        /* Сlick on delete filter */
        .on('click', '.da-filter-remove', daFilterRemove)
        /* Change limit on page */
        .on('click', '.da-limit', daLimit)
        /* Change ordering on columns */
        .on('click', '.da-sort', daSort)
        /* Click on pagination */
        .on('click', '.da-pagination li:not(.disabled) a:not(.disabled), .da-pagination-stack a:not(.disabled)', daPagination)
        /* Click on refresh */
        .on('click', '.da-refresh', daRefresh)
        /* Click on toggles */
        .on('click', '[data-toggle^=pane-filters],[data-toggle^=pane-info],[data-toggle^=pane-columns]', simpleToggle)
        .on('click', '[data-toggle=table-type]', daTableType)
        /* Toggle column */
        .on('click', '.da-column-visible', daColumnVisible)
        /* Change column display name */
        .on('change', '.da-column-name', daColumnName)
        /* Change column width */
        .on('change', '.da-column-width', daColumnWidth)
}

