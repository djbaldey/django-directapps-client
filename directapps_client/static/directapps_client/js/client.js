/*
 * client.js
 * 
 * Запуск приложения.
 * 
 * Зависит от:
 *  js/globalvars.js
 *  js/localstorage.js
 *  js/links.js
 *  js/menu.js
 *  js/tabs.js
 *  js/masks.js
 *  js/handlers.js
 * 
 */

function mainApp() {
    menuInit();
    tabsInit();
    masksInit();
    handlersInit();
    filtersInit();
    fieldsInit();
    // Начальной страницей является #dashboard.
    // При открытии других страниц, запускается обработчик указанного хеша.
    if (!location.hash || location.hash == '#') {
        location.hash = '#dashboard'
        $('#sidebar-menu-filter').focus()
    } else {
        window.onhashchange()
    }
    /* Вызов AJAX загрузки следующей страницы для стековых таблиц
     * при прокрутке до 100px от низа страницы.
     */
    $(window).scroll(function() {
        var $w = $(window);
        if($(document).height() - $w.height() - $w.scrollTop() <= 100 ) {
            $('#tab-panes>div.tab-pane.active .da-pagination-stack a:visible:not(.loading)')
                .addClass('loading').click()
        }
    });

}

$(document).ready(function($) {
    window.TABS = [];
    window.HASH_QUEUE = [];
    window.CACHE = fromLocalStorage('cache') || {};
    window.CONFIG = fromLocalStorage('config') || {};
    window.HISTORY = fromLocalStorage('history') || [];
    window.FAVORITES = fromLocalStorage('favorites') || [];
    window.SIMPLE_TOGGLES = fromLocalStorage('simple_toggles') || {};
    window.TEMPLATES = internalTemplates();
    window.UTC_OFFSET = moment().format('Z');
    var TS = window.TRANSLATIONS || {};
    if (TS.moment) {
        moment.updateLocale($('html').attr('lang') || 'ru', TS.moment)
    }
    var defaults = $.fn.datetimepicker.defaults;
    defaults['locale'] = moment.locale();
    defaults['format'] = 'YYYY-MM-DD HH:mm:ss';
    defaults['useCurrent'] = false;
    if (TS.datepicker_tooltips) {
        defaults['tooltips'] = TS.datepicker_tooltips
    }
    defaults['icons'] = {
        time: 'fa fa-fw fa-clock-o',
        date: 'fa fa-fw fa-calendar',
        up: 'fa fa-fw fa-chevron-up',
        down: 'fa fa-fw fa-chevron-down',
        previous: 'fa fa-fw fa-chevron-left',
        next: 'fa fa-fw fa-chevron-right',
        today: 'fa fa-fw fa-crosshairs',
        clear: 'fa fa-fw fa-trash',
        close: 'fa fa-fw fa-close'
    }; 
    loadMenu(mainApp);
})
