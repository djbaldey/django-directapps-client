/*
 * pagination.js
 * 
 * Прорисовка различных паджинаторов.
 * 
 */

/* Twitter bootstrap .pager as stack */
function bsStack(id, page, num_pages) {
    var $p = $('#pager-'+id),
        $a = $('#pager-'+id+' a:not(.disabled)');
    if (page < num_pages) {
        $a.data('page', (page+1)).removeClass('loading');
        $p.show();
        if ($(document).height() <= $(window).height()) {
            // автоматическая подгрузка следующей страницы,
            // если документ меньше окна браузера
            $a.addClass('loading').click()
        }
    } else {
        $a.removeClass('loading').data('page', '');
        $p.hide();
    }
};

/* Twitter bootstrap .pager inner HTML */
function bsPager(id, page, num_pages, hide_prev) {
    var html = '', tp = '&laquo;', tn = '&raquo;';
    if (page >1 && !hide_prev) {
        html += '<li><a href="#" data-page="'+(page-1)+'">'+tp+'</a></li>';
    } else {
        html += '<li class="disabled"><span>'+tp+'</span></li>';
    }
    if (page < num_pages) {
        html += '<li><a href="#" data-page="'+(page+1)+'">'+tn+'</a></li>';
    } else {
        html += '<li class="disabled"><span>'+tn+'</span></li>';
    }
    $('#pager-'+id).html(html);
};

/* Twitter bootstrap .pagination inner HTML */
function bsPagination(id, page, num_pages, on_each_side, on_ends) {
    var html = '',
        tp = '&laquo;',
        tn = '&raquo;',
        on_each_side = on_each_side || 3,
        on_ends = on_ends || 2,
        dot = '.',
        page_range = [];

    //~ if (page >1) {
        //~ html += '<li><a href="#" data-page="'+(page-1)+'">'+tp+'</a></li>';
    //~ } else {
        //~ html += '<li class="disabled"><span>'+tp+'</span></li>';
    //~ }

    var _push = function(s, e) { for(var i=s; i<e; i++) { page_range.push(i) } };

    if (num_pages > 9) {

        if (page > (on_each_side + on_ends)) {
            _push(1, on_each_side);
            page_range.push(dot);
            _push(page+1-on_each_side, page+1);
        } else {
            _push(1, page+1)
        }

        if (page < (num_pages - on_each_side - on_ends + 1)) {
            _push(page+1, page+on_each_side);
            page_range.push(dot);
            _push(num_pages-on_ends+1, num_pages+1);
        } else {
            _push(page+1, num_pages+1)
        }
    } else {
        page_range = $.map($(Array(num_pages)), function(val, i) { return i+1; })
    };

    $.each(page_range, function(i, item) {
        if (item == dot) {
            html += '<li class="disabled"><span>...</span></li>';
        } else if (item == page) {
            html += '<li class="active"><span>'+page+'</span></li>';
        } else {
            html += '<li><a href="#" data-page="'+item+'">'+item+'</a></li>';
        }
    });

    //~ if (page < num_pages) {
        //~ html += '<li><a href="#" data-page="'+(page+1)+'">'+tn+'</a></li>';
    //~ } else {
        //~ html += '<li class="disabled"><span>'+tn+'</span></li>';
    //~ }

    $('#pager-'+id).html(html);
}

/* Распределяет запросы к различным паджинаторам. */
function renderPage(id, table_type, page, num_pages) {
    if (table_type == 'stack') {
        return bsStack(id, page, num_pages)
    }
    else if (table_type == 'pager') {
        return bsPager(id, page, num_pages)
    }
    return bsPagination(id, page, num_pages);
}


