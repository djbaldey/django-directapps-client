/*
 * directapps.js
 * 
 * Обработка ссылок DirectApps.
 * 
 * Зависит от:
 *  js/localstorage.js,
 *  js/utils.js,
 * 
 */

// Регистр готовых контроллеров.
var CONTROLLERS = {};


/* Функция проверяет принадлежность ссылки к DirectApps. */
function isDirectAppsLink(hash) {
    return hash.indexOf('#!' + APPS_URL) == 0
}


/* Функция возвращает список из частей URL для DirectApps, по которому можно
 * определить для чего ссылка - для приложения, модели, объекта или связи. 
 * Например: ['/apps', 'auth', 'user', '1', 'orders'].
 */
function directAppsLinks(hash) {
    if (!isDirectAppsLink(hash)) return;
    var rel = hash.split(APPS_URL)[1];
    if (!rel) return;
    var links = [APPS_URL.slice(0, -1)],
        rels = rel.split('/');
    for (var i = 0; i < 4; i++) {
        if (!rels[i] || rels[i].indexOf('_') == 0) break;
        links.push(rels[i])
    }
    return links
}


/* Функция добавляет в кэш обновлённую схему приложения. */
function appToCache(obj) {
    var apps = window.CACHE.scheme.apps,
        l = apps.length;
    for (var i = 0; i < l; i++) {
        var app = apps[i];
        if (app.name == obj.name) {
            apps[i] = obj;
            toLocalStorage('cache', window.CACHE);
            return obj
        }
    }
}


/* Функция возвращает из кеша схему приложения по её имени. */
function getApp(name) {
    var apps = window.CACHE.scheme.apps,
        l = apps.length;
    for (var i = 0; i < l; i++) {
        var app = apps[i];
        if (app.name == name) {
            return app
        }
    }
}


/* Функция возвращает схему модели приложения по её имени. */
function getModel(app, name) {
    var m, ms = app.models, l = ms.length;
    for (var i = 0; i < l; i++) {
        m = ms[i];
        if (m.name == name) {
            return m;
        }
    }
}


/* Функция возвращает схему модели приложения по её имени. */
function getRelation(model, name) {
    var r, rs = model.object.relations, l = rs.length;
    for (var i = 0; i < l; i++) {
        r = rs[i];
        if (r.name == name) {
            return r
        }
    }
}


/* Функция формирует хеш для объекта. */
function makeObjectHash(relation, object_id) {
    var app, model, names = relation.split('.');
    app = getApp(names[0]);
    // Пользователь сообщит о перезагрузке страницы, если есть ошибка
    // в реализации клиента.
    if (!app) return window.location.pathname;
    model = getModel(app, names[1]);
    if (!model) return window.location.pathname;
    return '#!' + model.url + object_id + '/';
}


/* Рекурсивная функция, возвращающая схемы элементов DirectApps.
 * Изначально, в кэше хранятся неполные схемы приложений, достаточные
 * лишь для создания навигации по проекту.
 * 
 * Первым этапом получаем схему приложения, укомплектованную
 * всеми схемами объектов и их моделей. Схемы объектов, при этом содержат
 * только заголовки для отношений, например: ['users', 'Пользователи'].
 * Схема самого отношения должна располагаться третьим параметром, поэтому
 * её получение - это второй этап обращения к серверу.  
 * 
 */
function loadScheme(app, model, relation, cb) {
    if (typeof app === 'string') {
        app = getApp(app);
        if (app && !app.complete) {
            $.getJSON(APPS_URL + app.name + '/', function(scheme) {
                app = appToCache(scheme);
                loadScheme(app, model, relation, cb)
            });
            return
        }
    }
    if (!app) {
        throw new TypeError('App not found.');
        return
    }
    if (model && typeof model === 'string') {
        model = getModel(app, model) || model
    }
    if (model && typeof model === 'string') {
        throw new TypeError('Model not found.');
        return
    }
    if (model && model.name && relation && typeof relation === 'string') {
        var rel = getRelation(model, relation);
        if (!rel) {
            throw new TypeError('Relation not found.');
            return
        } else if (!rel.page_key || !rel.limit_key || !rel.ordering_key) {
            $.getJSON(APPS_URL + app.name + '/' + model.name + '/0/' + relation + '/_scheme/',
                function(scheme) {
                    $.extend(rel, scheme);
                    app = appToCache(app);
                    loadScheme(app, model, rel, cb)
                }
            );
            return
        } else {
            relation = rel;
        }
    }
    if (relation && typeof relation === 'string') {
        throw new TypeError('Relation not found.');
        return
    }
    if (cb) return cb(app, model, relation);
    else return [app, model, relation];
}


/* Функция, устанавливающая и возвращающая конфиг модели (или связанной). */
function getConfig(app_name, model_name, relation_name) {
    if (!app_name || !model_name) return;
    var save = false, fullconf = window.CONFIG || {};
    if (!fullconf[app_name]) {
        fullconf[app_name] = {};
        save = true
    }
    var app = fullconf[app_name];
    if (!app[model_name]) {
        app[model_name] = defaultConf();
        save = true
    }
    var model = app[model_name];
    if (relation_name) {
        if (!model.relations) model.relations = {};
        if (!model.relations[relation_name]) {
            model.relations[relation_name] = defaultConf();
            save = true
        }
    }
    if (save) saveConfig(fullconf);
    if (relation_name) return model.relations[relation_name];
    return model
} 


/* Функция, возвращающая тип сортировки поля. */
function getOrdering(model, config, name) {
    var use_default_ordering = !config.ordering[0];
    // Возвращает сортировку для колонки:
    // undefined - не сортируется
    // true - сортируется, но не задана
    // 'asc' - задана по возрастанию
    // 'desc' - задана по убыванию
    if (model.order_columns.indexOf(name) > -1) {
        if (use_default_ordering) {
            if (model.default_ordering.indexOf(name) > -1) return 'asc';
            if (model.default_ordering.indexOf('-' + name) > -1) return 'desc';
        } else {
            if (config.ordering.indexOf(name) > -1) return 'asc';
            if (config.ordering.indexOf('-' + name) > -1) return 'desc';
        }
        return true
    }
}


/* Функция, проверяющая права доступа к моделям. */
function testPermission(relation, perm) {
    if ($.isArray(relation))
        relation = relation[0] + '.' + relation[1];

    var model, ctrl = CONTROLLERS[relation];

    if (ctrl) {
        model = ctrl.model
    } else {
        var names = relation.split('.'),
            app = getApp(names[0]);
        if (!app) return false;
        model = getModel(app, names[1]);
        if (!model) return false;
    }

    var perms = model.perms;
    if ((!perm && perms[0]) || perms == 'all') return true;
    if (!$.isArray(perm)) perm = [perm];
    // любое из разрешений в списке возвращает истину
    for (var i = 0; i < perm.length; i++) {
        if (perms.indexOf(perm[i]) > -1) {
            return true
        }
    }
    return false
}


/* Класс мастер-контроллера. */
function MasterCtrl(app, model) {
    this.name = app.name + '.' + model.name;
    this.app = app;
    this.model = model;
    this.newobject_url = model.url + '0/';
    // здесь и далее в словарях: '_' - это ключ модели, остальные ключи - реляции
    this.requests = {};
    this.pages = {};
    this.config = getConfig(app.name, model.name);
    this._relations = {}; // для быстрого доступа по имени.

    var self = this;

    /* Возвращает схему связанной модели. */
    this.getRel = function(name) {
        var rs = self._relations,
            rel = rs[name];
        if (!rel) rs[name] = getRelation(model, name);
        return rs[name]
    }

    /* Создаёт и возвращает конфиг для связанной модели. */
    this.getRelConfig = function(rel) {
        if (!rel) throw new TypeError('Relation not found.');
        var name = rel.name;
        if (!self.config.relations || !self.config.relations[name]) {
            return getConfig(app.name, model.name, name)
        }
        return self.config.relations[name]
    }

    /* Запрос коллекции элементов. */
    this.get = function() {
        var rs = self.requests, name = '_';
        if (rs[name]) rs[name].abort();

        var c = self.config,
            page = self.pages[name],
            data = requestFilters(c),
            limit = c.limit;

        if (limit != model.limit) data[model.limit_key] = limit;
        if (page > 1) data[model.page_key] = page;
        if (c.ordering.length) {
            data[model.ordering_key] = c.ordering.join(',');
        }
        var xhr = $.getJSON(model.url, data);
        rs[name] = xhr;
        return xhr.always(function() {rs[name] = null})
    }

    /* Запрос коллекции элементов связанной модели. Возвращает jqxhr. */
    this.getRelation = function(pk, name) {
        var rs = self.requests, page = self.pages[name];
        if (rs[name]) rs[name].abort();

        var rel = self.getRel(name),
            c = self.getRelConfig(rel),
            data = requestFilters(c),
            limit = c.limit;

        if (limit != rel.limit) data[rel.limit_key] = limit;
        if (page > 1) data[rel.page_key] = page;
        if (c.ordering.length) {
            data[rel.ordering_key] = c.ordering.join(',');
        }
        var url = model.url + pk + '/' + name + '/',
            xhr = $.getJSON(url, data);
        rs[name] = xhr;
        return xhr.always(function() { rs[name] = null })
    }

    /* Запрос одного объекта. */
    this.getObject = function(pk) {
        return $.getJSON(model.url + pk + '/')
    }

    /* Возвращает новый объект с заполнением полей по-умолчанию. */
    this.newObject = function() {
        var object = {
            display_name: model.object.display_name,
            fields: {}
        }
        // автозаполнение полей.
        $.each(model.object.fields, function(i, field) {
            var value,
                jsfield = FIELDS[field.type] || FIELDS.Field;
            if (field.has_default) {
                value = jsfield.getDefault(field['default'])
            }
            object.fields[field.name] = value
        })
        return object
    }

    /* Создание одного объекта. */
    this.createObject = function(data) {
        return $.ajax({
            dataType: "json",
            type: 'POST',
            url: model.url,
            data: data
        })
    }

    /* Обновление нескольких объектов за раз. */
    this.updateObjects = function(list_pk, data) {
        data['id'] = list_pk.join(',');
        return $.ajax({
            dataType: "json",
            type: 'PUT',
            url: model.url,
            data: data
        })
    }

    /* Обновление одного объекта. */
    this.updateObject = function(pk, data) {
        return $.ajax({
            dataType: "json",
            type: 'PUT',
            url: model.url + pk + '/',
            data: data
        })
    }

    /* Удаление нескольких объектов за раз. */
    this.deleteObjects = function(list_pk) {
        return $.ajax({
            dataType: "json",
            type: 'DELETE',
            url: model.url,
            data: {'id': list_pk.join(',')}
        })
    }

    /* Удаление одного объекта. */
    this.deleteObject = function(pk) {
        return $.ajax({
            dataType: "json",
            type: 'DELETE',
            url: model.url + pk + '/'
        })
    }

    /* Возвращает конфиг и схему модели реляции или основной. */
    this.mc = function(rel) {
        var c, m;
        if (!rel) {
            c = self.config;
            m = model;
        } else {
            if (typeof rel === 'string') {
                rel = self.getRel(rel)
            }
            c = self.getRelConfig(rel);
            m = rel;
        }
        return [m, c]
    }

    /* Возвращает список видимых колонок с правилами их сортировки (или без). */
    this.visibleColumns = function(rel, with_sorting) {
        var mc = self.mc(rel),
            m = mc[0],
            c = mc[1],
            cols = [];

        $.each(c.visible_columns, function(i, col) {
            var _col = $.extend({
                custom_name: (c.colnames||{})[col.name],
                width: (c.colwidth||{})[col.name]
            }, col);
            if (with_sorting) {
                _col.sorting = getOrdering(m, c, col.name)
            }
            cols.push(_col)
        })
        return cols
    }

    /* Изменяет порядок сортировки колонок. */
    this.changeOrdering = function(name, rel) {
        var mc = self.mc(rel),
            m = mc[0],
            c = mc[1],
            state = getOrdering(m, c, name);

        if (state) {
            var asc = (state == 'asc'),
                desc = (state == 'desc'),
                post = asc ? '-' + name : desc ? null : name;
            if (c.multiordering) {
                var col = asc ? name : desc ? '-' + name : null,
                    i = c.ordering.indexOf(col);
                if (i > -1) {
                    if (post) {
                        c.ordering[i] = post
                    } else {
                        c.ordering.splice(i, 1)
                    }
                } else if (post) {
                    c.ordering.push(post)
                }
            } else {
                if (post) {
                    c.ordering[0] = post
                } else {
                    c.ordering = [];
                }
            }
            saveConfig();
        }

        return state
    }

    /* Проверяет наличие включенных фильтров за исключением фильтра
     * глобального поиска.
     */
    this.testFilters = function(rel) {
        var mc = self.mc(rel),
            m = mc[0],
            c = mc[1]
            fs = c.filters;
        for (var i = 0; i < fs.length; i++) {
            var f = fs[i];
            if (f.name != m.search_key && f.active) return true
        }
        return false
    }

    /* Проверяет наличие разрешений. */
    this.testPermission = function(perm, relation) {
        var names;
        if (relation) {
            if (relation.indexOf('.') < 0) {
                relation = (self.getRel(relation) || {}).relation
            }
            if (!relation) return false;
            names = relation.split('.')
        } else {
            names = self.name.split('.')
        }
        // Краткие встроенные разрешения
        var internals = ['view', 'add', 'change', 'delete'],
            i = internals.indexOf(perm);
        if (i > -1) {
            perm = names[0] + '.' + internals[i] + '_' + names[1]
        } else if (perm.indexOf('.') < 0) {
            perm = names[0] + '.' + perm
        }
        return testPermission(names.join('.'), perm)
    }

    // Преобразование разрешений в CRUD.
    this.CRUD = {
        create: this.testPermission('add'),
        read: this.testPermission('view'),
        update: this.testPermission('change'),
        'delete': this.testPermission('delete')
    }

    this.get_form_fieldsets = function() {
        if (!self._form_fieldsets) {
            self._form_fieldsets = formFieldSets(
                $.extend(true, [], self.model.object.fields))
        }
        return self._form_fieldsets
    }

}


/* Функция, создающая и возвращающая объект мастер-контроллера. */
function getController(app, model) {
    // app - это либо объект приложения, либо имя контроллера.
    if (!model) return CONTROLLERS[app];
    var name = app.name + '.' + model.name;
    if (!CONTROLLERS[name]) CONTROLLERS[name] = new MasterCtrl(app, model);
    return CONTROLLERS[name]
}
