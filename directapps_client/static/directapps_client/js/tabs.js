/*
 * tabs.js
 * 
 * Создание и открытие вкладок
 * 
 * Зависит от:
 *  js/templates.js
 * 
 */

/* Функция проверяет существование вкладки. */
function existTab(id) {
    return ($('#content-tabs a[href="#pane-'+id+'"]').length)
}

/* Функция создаёт вкладку и область под данные. */
function createTab(id, name, hash) {
    var TS = window.TEMPLATES;
    if (!existTab(id)) {
        window.TABS.push([id, name, hash]);
        toLocalStorage('tabs', window.TABS);
        var d = {id: id, name: (name || 'new tab').capFirst(), hash: hash};
        $('#content-tabs').append(TS.tab(d));
        $('#tab-panes').append(TS.pane(d))
        return true
    }
    return false
}

/* Функция переименовывает вкладку. */
function renameTab(id, name) {
    var qs = $('#content-tabs a[href="#pane-'+id+'"]');
    name = name.capFirst();
    qs.data('title', name);
    qs.find('span.long').text(name.truncateWords(15));
    qs.find('span.short').text(name.truncateChars(3, true));
    if (window.PROJECT_NAME) name = PROJECT_NAME + ': ' + name;
    $('title').text(name)
}

/* Функция открывает вкладку. */
function openTab(id) {
    var data = $('#content-tabs a[href="#pane-'+id+'"]').tab('show').data(),
        name = data.title,
        hash = data.hash
        queue = window.HASH_QUEUE;
    var i = queue.indexOf(hash);
    if (i > -1) queue.splice(i, 1);
    queue.push(hash);
    if (name) {
        if (window.PROJECT_NAME) name = PROJECT_NAME + ': ' + name;
        $('title').text(name)
    }
}

/* Функция переоткрывает вкладку. */
function reopenTab(id) {
    var $a = $('#content-tabs a[href="#pane-'+id+'"]');
    $($a.attr('href')).remove();
    $a.parent().remove();
    window.onhashchange()
}

/* Обработчик закрывает вкладку. */
function closeTab(id) {
    var $a = $('#content-tabs a[href="#pane-'+id+'"]'),
        data = $a.data();
    $($a.attr('href')).remove();
    $a.parent().remove();
    var tabs = window.TABS;
    for (var i=0;i<tabs.length;i++) {
        if (tabs[i][0] == id) {
            tabs.splice(i, 1);
            break
        }
    }
    toLocalStorage('tabs', tabs);
    var queue = window.HASH_QUEUE,
        i = queue.indexOf(data.hash);
    if (i > -1) queue.splice(i, 1);
    var prev = queue[queue.length-1];
    if (prev) {
        location.hash = prev;
    } else {
        $('#content-tabs a:last').tab('show');
    }
}

/* Инициализация */
function tabsInit() {
    var tabs = fromLocalStorage('tabs') || [];
    $.each(tabs, function(i, tab) {
        createTab(tab[0], tab[1], tab[2]);
    })
    $('#content-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        if (e.target) {
            var data = $(e.target).data();
            if (data.hash) location.hash = data.hash
        }
    })
    $('body').on('click', '#content-tabs .close', function(e) {
        closeTab($(this).data('id'))
    })
}

