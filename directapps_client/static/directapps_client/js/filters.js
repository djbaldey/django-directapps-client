/*
 * filters.js
 * 
 * Обработка фильтров.
 * 
 */

/* Функция возвращающая фильтр из списка фильтров. При необходимости
 * автоматически добавляет пустой фильтр в этот список. */
function getFilterByName(config, name, orCreate) {
    var fs = config.filters
    if (name) {
        for (var i=0;i<fs.length;i++) {
            var f = fs[i];
            if (f.name == name) return f
        }
    }
    if (orCreate) {
        var f = { name: name, id: generatorID('filter') };
        fs.push(f);
        return f
    }
}

/* Функция аналогичная getFilterByName, только по идентификатору. */
function getFilterById(config, id, orCreate) {
    var fs = config.filters
    if (id) {
        for (var i=0;i<fs.length;i++) {
            var f = fs[i];
            if (f.id == id) return f
        }
    }
    if (orCreate) {
        var f = { id: id || generatorID('filter') };
        fs.push(f);
        return f
    }
}

/* Функция удаляющая фильтр из списка фильтров по имени фильтра. */
function deleteFilterByName(config, name) {
    var fs = config.filters
    for (var i=0;i<fs.length;i++) {
        var f = fs[i];
        if (f.name == name) {
            fs.splice(i, 1);
            return
        }
    }
}

/* Функция удаляющая фильтр из списка фильтров по идентификатору. */
function deleteFilterById(config, id) {
    var fs = config.filters
    for (var i=0;i<fs.length;i++) {
        var f = fs[i];
        if (f.id == id) {
            fs.splice(i, 1);
            return
        }
    }
}

/* Функция возвращающая объект данных запроса для всех активных фильтров. */
function requestFilters(config) {
    var data = {};

    $.each(config.filters, function(i, f) {
        if (f.name && f.active != false) {
            var is_range, is_list,
                v = f.value, t = $.type(v);
            if (f.condition) {
                is_range = f.condition.endsWith('range');
                is_list = f.condition.endsWith('in');
            }
            if (is_range || is_list) {
                var ft = $.type(v[0])
                if (ft === 'array') {
                    v = $.map(v, function(arr, i) { return arr[0] })
                } else if (ft === 'date') {
                    v = $.map(v, function(val, i) { return val.toISOString() })
                }
                v = '['+v.join(',')+']'
            } else if (t === 'array') {
                v = v[0]
            } else if (t === 'date') {
                v = v.toISOString()
            }
            data[f.name] = v
        }
    })
    return data
}

/* Вспомогательная функция, создающая строку данных для HTML-тега фильтра. */
function filterDataString(id, ctrl, object, relation, filter) {
    return 'data-id="'+id
        +'" data-filter="'+filter.id
        +(object? '" data-object="'+(object.pk || object) : '')
        +(relation? '" data-relation="'+(relation.name || relation): '')
        +'" data-ctrl="'+ctrl.name+'"';
}

/* Формирует список готовых значений, где каждое состоит из серверного варианта
 * и человекочитаемым отображением.
 */
function getFilterValues(filter, field) {
    var vals = [];
    if (filter.value == undefined || filter.value == null) return vals;

    function humanize(v) {
        if (String(v) == 'true') return TRANSLATIONS['true'];
        if (String(v) == 'false') return TRANSLATIONS['false'];
        if (v == 'yes') return TRANSLATIONS['yes'];
        if (v == 'no') return TRANSLATIONS['no'];
        return v
        //~ return displayValue(v, filter, field)
    }

    var is_range = filter.condition.endsWith('range'),
        is_list = filter.condition.endsWith('in'),
        is_null = filter.condition.endsWith('isnull');
    if ((is_range || is_list)) {
        var first = filter.value[0];
        if ($.type(first) == 'array') {
            vals = filter.value
        } else {
            vals = $.map(filter.value, function(v, i) { return [v, humanize(v)] })
        }
    } else if ($.type(filter.value) == 'array') {
        vals[0] = filter.value
    } else if (is_null) {
        vals[0] = [filter.value, field.labels['isnull'][Number(filter.value) || 0]]
    } else if (field.is_bool) {
        vals[0] = [filter.value, field.labels['exact'][Number(filter.value) || 0]]
    } else if (filter.value) {
        vals[0] = [filter.value, humanize(filter.value)]
    }
    return vals
}

/* Устанавливает имя фильтра в конфиге. */
function setFilterName(filter) {
    var n = filter.inverse? '-' : '';
    n += filter.field
    if (filter.condition != 'exact') {
        n += '__'+filter.condition
    }
    filter.name = n
    return n
}

/* Устанавливает значение фильтра в конфиге. */
function setFilterValue(filter, value) {
    var is_range = filter.condition.endsWith('range'),
        is_list = filter.condition.endsWith('in');

    setFilterName(filter);
    filter.active = false;

    if (value && (is_range || is_list)) {
        if (!filter.value) {
            filter.value = []
        }
        if (filter.value.indexOf(value) < 0) {
            filter.value.push(value)
        }
        if (is_list || filter.value.length == 2) {
            filter.active = true
        }
    } else {
        filter.value = value
        if (value != undefined && value != null) {
            filter.active = true
        }
    }

    saveConfig()
}

/* Удаляет значение фильтра в конфиге. */
function deleteFilterValue(filter, value) {
    var is_range = filter.condition.endsWith('range'),
        is_list = filter.condition.endsWith('in');

    filter.active = false;

    var is_range = filter.condition.endsWith('range'),
        is_list = filter.condition.endsWith('in');

    if (is_range || is_list) {
        for (var i=0;i<filter.value.length;i++) {
            var val = filter.value[i];
            if ($.type(val) == 'array') {
                val = val[0]
            }
            if (val == value) {
                filter.value.splice(i, 1);
                break
            }
        }
        if (is_list && filter.value.length > 0) {
            filter.active = true
        }
    } else {
        delete filter.value
    }

    saveConfig()
}

/* Проверка неполной готовности фильтра для отображения поля ввода значения. */
function visibleFilterControls(filter, field) {
    if (filter.value == undefined || filter.value == null) {
        return true
    } else if (filter.condition.endsWith('in')) {
        return true
    } else if (filter.condition.endsWith('range') && filter.value.length < 2) {
        return true
    }
    return false
}

function setTypeaheadFilter(e) {
    var $input = $(this),
        data = $input.data(),
        // id = data.id,
        ctrl = getController(data.ctrl),
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter);

    var afterSelect = function(current) {
        if (current) {
            $input.blur()
                .data('server_value', current.pk)
                .parent().find('.filter-value-save').focus()
        } else {
            $input.data('server_value', '').val('')
        }
    }

    $input.removeClass('typeahead-is-not-set').typeahead({
        source: function(query, callback) {
            var url = ctrl.model.url+(rel? ('0'+'/'+rel.name+'/') : '')+'_fkey/',
                params = {q: query, f: filter.field};
            $.getJSON(url, params, callback);
        },
        matcher: function() {return true},
        items: 'all',
        displayText: function(item) {return item.display_name || item},
        afterSelect: afterSelect
    })
    .keydown(function(e) {
        if (e.which == 8 && $input.data('server_value')) {
            afterSelect()
        }
    })
}

function setDateTimePickerFilter(e) {
    var $input = $(this),
        data = $input.data();
    $input.removeClass('datetimepicker-is-not-set')
          .datetimepicker({format: data.format})
          .data("DateTimePicker")
          .show();

    $input.on("dp.change", function(e) {
        var sv = moment(this.value, data.format).toISOString();
        $(this).data('server_value', sv)
    })
}

function hasDateTimePickerFilter(cond) {
    var ex = ['year', 'month', 'day', 'week_day', 'hour', 'minute', 'second'];
    for (var i=0;i<ex.length;i++) {
        if (cond.startsWith(ex[i])) return false
    }
    return true
}

/* Подсветка кнопки фильтров */
function colorFiltersBtn(id, ctrl, rel) {
    var aclass, rclass;
    if (ctrl.testFilters(rel)) {
        aclass = 'btn-danger';
        rclass = 'btn-default';
    } else {
        aclass = 'btn-default';
        rclass = 'btn-danger';
    }
    $('[data-toggle=pane-filters-'+id+']').addClass(aclass)
                                            .removeClass(rclass)
}

function filterValueSave(e) {
    var $this = $(this).blur(),
        $input = $this.parent().siblings('input,select'),
        as_choices = $input.data('as_choices'),
        server_value = $input.data('server_value'),
        value = $input.val(),
        data = $this.data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter),
        page = 1,
        replace = true;

    if (value) {
        // boolean strings
        if (value == 'true') {
            value = true
        } else if (value == 'false') {
            value = false
        }

        if (as_choices) {
            value = [value, $input.find('option:selected').text()]
        }
        else if (server_value) {
            value = [server_value, value]
        }
        setFilterValue(filter, value);
        $('.da-filter-values[data-filter="'+filter.id+'"]').html(
            renderFilterValues(id, ctrl, config, pk, rel, filter)
        )

        if (relation) {
            renderRelation(id, ctrl, pk, relation, page, replace)
        } else {
            renderModel(id, ctrl, page, replace)
        }
    }
    colorFiltersBtn(id, ctrl, rel);

}

function filterValueClose(e) {
    var $this = $(this).blur(),
        $button = $this.parent(),
        value = $button.val(),
        data = $button.data(),
        id = data.id,
        ctrl = getController(data.ctrl),
        pk = data.object,
        relation = data.relation,
        rel = ctrl.getRel(relation),
        config = relation ? ctrl.getRelConfig(rel) : ctrl.config,
        filter = getFilterById(config, data.filter),
        page = 1,
        replace = true;

    if (value) {
        deleteFilterValue(filter, value);
        $('.da-filter-values[data-filter="'+filter.id+'"]').html(
            renderFilterValues(id, ctrl, config, pk, rel, filter)
        )
        if (relation) {
            renderRelation(id, ctrl, pk, relation, page, replace)
        } else {
            renderModel(id, ctrl, page, replace)
        }
    }

    colorFiltersBtn(id, ctrl, rel);
}

/* Инициализация */
function filtersInit() {
    $('body').on('focus', '.da-filter-values .datetimepicker-is-not-set', setDateTimePickerFilter)
    $('body').on('focus', '.da-filter-values .typeahead-is-not-set', setTypeaheadFilter)
    $('body').on('click', '.filter-value-save', filterValueSave)
    $('body').on('click', '.filter-value-close', filterValueClose)
}

