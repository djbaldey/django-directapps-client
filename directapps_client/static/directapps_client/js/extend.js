/*
 * extend.js
 *
 * Расширение стандартных объектов JavaScript.
 *
 */


/* Проверка начала строки */
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(test) {
        return this.IndexOf(test) == 0
    }
}


/* Проверка окончания строки */
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(test) {
        return this.lastIndexOf(test) + test.length == this.length
    }
}


/* Перевод в верхний регистр первой буквы текста. */
if (!String.prototype.capFirst) {
    String.prototype.capFirst = function() {
        if (this.length == 0) return this;
        return this[0].toUpperCase() + this.slice(1)
    }
}


/* Обрезка строки до заданного количества символов.
 * Если второй параметр является истиной, то многоточие не добавляется.
 */
if (!String.prototype.truncateChars) {
    String.prototype.truncateChars = function(l, d) {
        var s = this;
        if (s && l && s.length > l) return s.slice(0, l) + (d ? '' : '...');
        return s
    }
}


/* Обрезка строки до заданного количества слов.
 * Если второй параметр является истиной, то многоточие не добавляется.
 */
if (!String.prototype.truncateWords) {
    String.prototype.truncateWords = function(l, d) {
        var s = this;
        if (!s) return s;
        s = s.split(/\s+/);
        if (l && s.length > l) return s.slice(0, l).join(' ') + (d ? '' : '...');
        return s.join(' ')
    }
}


/* Дополнение строк слева (по-умолчанию пробелы). */
if (!String.prototype.leftPad) {
        String.prototype.leftPad = function(l, c) {
        var s = '',
            c = c || ' ',
            l = (l || 2) - this.length;
        while (s.length < l) s += c;
        return s + this;
    }
}


/* Преобразование чисел в строки с дополнением (по-умолчанию нули). */
if (!Number.prototype.leftPad) {
    Number.prototype.leftPad = function(l, c) {
        return String(this).leftPad(l, c || '0');
    }
}
