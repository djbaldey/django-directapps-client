/*
 * utils.js
 * 
 * Утилиты общего назначения.
 * 
 */


/* Функция сортировки объектов в списке по ключу `display_name`. */
function sortDisplayName(a, b) {
    if (a.display_name > b.display_name) return 1;
    return -1;
}


/* Приводит идентификаторы в позволительный jQuery вид.
 * В данном приложении он заменяет точки на "__".
 * На вход может принимать список или строку
 */
function validatorID(id) {
    if ($.type(id) === 'array') { id = id.join('-') };
    return id.replace(/[\.,\:,\/, ,\(,\),=,?]/g, "__");
};


/* Генератор идентификаторов, которому можно задавать статические
 * начало и конец идентификатора, например:
 *  >> id = generatorID()
 *  >> "i1363655293735"
 *  >> id = generatorID(null, "object")
 *  >> "i1363655293736_object"
 *  >> id = generatorID("object")
 *  >> "object_i1363655293737"
 *  >> id = generatorID("model", "object")
 *  >> "model_i1363655293738_object"
 */
function generatorID(prefix, postfix) {
    var result = [],
        gen = prefix? '' : 'id',
        m = 1000,
        n = 9999,
        salt = Math.floor( Math.random() * (n - m + 1) ) + m;
    gen += $.now() + String(salt);
    if (prefix) { result.push(prefix)};
    result.push(gen); 
    if (postfix) { result.push(postfix) };
    return validatorID(result);
};


/* Генерирует UUID. */
function uuid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
}


/* Функция отображения чисел c точкой. */
function floatFormat(d, digits) {
    if (digits) {
        var pow = Math.pow(10, parseInt(digits));
        d = Math.round(parseFloat(d) * pow) / pow;
    }
    var formatter = new Intl.NumberFormat(window.LANGUAGE_CODE, {
        minimumFractionDigits: digits,
    });
    return formatter.format(parseFloat(d) || 0.0);
}


/* Функция отображения целых исел. */
function integerFormat(d) {
    var formatter = new Intl.NumberFormat(window.LANGUAGE_CODE);
    return formatter.format(parseInt(d) || 0);
}


/* Функция предназначена для перевода URL в корректный ID объекта.
 * Пробелы меняются на дефисы, а прямой слеш - на двойное подчёркивание.
 */
function urlToId(s) {
    if (s.startsWith('/')) s = s.slice(1);
    if (s[s.length-1] == '/') s = s.slice(0, s.length-1);
    s = s.replace(/\s+/g, '-');
    s = s.replace(/\//g, '__');
    return s
}


function testDateTime(s) {
    var re = /^(\d{4}|[+\-]\d{6})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:\.(\d{3}))?Z$/;
    return (re.exec(s))
}


function testDate(s) {
    var re = /^(\d{4}|[+\-]\d{6})-(\d{2})-(\d{2})$/;
    return (re.exec(s))
}


function testTime(s) {
    var re = /^(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?$/;
    return (re.exec(s))
}


function daysFromNow(s) {
    var then = moment(s),
        now  = new Date;
    // 24 hours, 60 minutes, 60 seconds, 1000 milliseconds
    return Math.round((now - then) / (1000 * 60 * 60 * 24)); // round the amount of days
}


function saveConfig(config) {
    toLocalStorage('config', config || window.CONFIG || {})
}


function visibleColumnNames(visible) {
    return $.map(visible, function(col, i) { return col.name }) 
}


function getModelFilterByName(rel_or_model, name) {
    var fs = rel_or_model.filters;
    for (var i=0;i<fs.length;i++) {
        var f = fs[i]
        if (f.name == name) return f
    }
}


// Установки по-умолчанию
function defaultConf() {
    return {
        limit: 10,
        filters: [],
        ordering: [],
        visible_columns: [],
        colnames: {}, // кастомные названия колонок
        colwidth: {}, // кастомная ширина колонок в пикселях
        multiordering: false
    }
}


/* Вспомогательная функция, создающая строку данных для HTML-тега. */
function stdDataString(id, ctrl, relation, object) {
    return 'data-id="'+id
        +'" data-relation="'+(relation? relation.name : '')
        +'" data-object="'+(relation? object.pk : '')
        +'" data-ctrl="'+ctrl.name+'"';
}
