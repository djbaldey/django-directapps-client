# -*- coding: utf-8 -*-
#
#   Copyright 2016 Grigoriy Kramarenko <root@rosix.ru>
#
#   This file is part of DjangoDirectAppsClient.
#
#   DjangoDirectAppsClient is free software: you can redistribute it and/or
#   modify it under the terms of the GNU Affero General Public License
#   as published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   DjangoDirectAppsClient is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public
#   License along with DjangoDirectAppsClient. If not, see
#   <http://www.gnu.org/licenses/>.
#
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.shortcuts import render, Http404
from django.template import TemplateDoesNotExist
from django.utils.encoding import force_text

from . import __version__
from .conf import CUSTOM_TEMPLATES, PROJECT_NAME


@login_required
def index(request):
    ctx = {
        'APPS_URL': reverse('directapps:apps'),
        'LOGOUT_URL': reverse('directapps_client:logout'),
        'CUSTOM_TEMPLATES': CUSTOM_TEMPLATES,
        'PROJECT_NAME': PROJECT_NAME,
        'DIRECTAPPS_CLIENT': __version__,
    }
    return render(request, 'directapps_client/index.html', ctx)


@login_required
def ajaxtemplate(request, name):
    try:
        return render(request, 'directapps_client/underscore/ajax/%s' % name)
    except TemplateDoesNotExist:
        raise Http404('The template file with the name `%s` was not '
                      'found in the ajax template directory.' % name)
