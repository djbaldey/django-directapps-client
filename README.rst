Django Direct Apps Client
=========================

This is one-page HTML client for a `django-directapps` application.
By default, acts as a quick admin panel Django. This application that
it can be easy to customize with override templates.

Installation
------------

.. code-block:: shell

    pip install django-directapps-client

Change your next project files.

.. code-block:: python

    # settins.py
    INSTALLED_APPS = (
        ...
        'directapps',
        'directapps_client',
        ...
    )

    # urls.py
    urlpatterns = [
        ...
        url(r'^apps/', include('directapps.urls', namespace="directapps")),
        url(r'^cli/', include('directapps_client.urls', namespace="directapps_client")),
        ...
    ]

Enjoy!

Contributing
------------
If you want to translate the app into your language or to offer a more
competent application code, you can do so using the "Pull Requests" on `github`_.

.. _github: https://github.com/rosix-ru/django-directapps-client/


